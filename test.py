	import os
	from pathlib import Path
	from ultralytics.utils.downloads import download
	from sahi.slicing import get_slice_bboxes
	from shapely.geometry import Polygon

  def visdrone2yolo(dir, train=False):
      from PIL import Image
      from tqdm import tqdm

      def convert_box(size, box):
          # Convert VisDrone box to YOLO xywh box
          dw = 1. / size[0]
          dh = 1. / size[1]
          return (box[0] + box[2] / 2) * dw, (box[1] + box[3] / 2) * dh, box[2] * dw, box[3] * dh

      (dir / 'labels').mkdir(parents=True, exist_ok=True)  # make labels directory
      pbar = tqdm((dir / 'annotations').glob('*.txt'), desc=f'Converting {dir}')
      for f in pbar:
          img_size = Image.open((dir / 'images' / f.name).with_suffix('.jpg')).size
          lines = []
          bboxes = []
          with open(f, 'r') as file:  # read annotation.txt
              for row in [x.split(',') for x in file.read().strip().splitlines()]:
                  if row[4] == '0':  # VisDrone 'ignored regions' class 0
                      continue
                  cls = int(row[5]) - 1
                  box = convert_box(img_size, tuple(map(int, row[:4])))
                  pos = tuple(map(int, row[:4]))
                  bboxes.append((cls, Polygon([(pos[0], pos[1]), (pos[0] + pos[2], pos[0]), (pos[0] + pos[2], pos[1] + pos[3]), (pos[0], pos[1] + pos[3])])))
                  lines.append(f"{cls} {' '.join(f'{x:.6f}' for x in box)}\n")
                  with open(str(f).replace(f'{os.sep}annotations{os.sep}', f'{os.sep}labels{os.sep}'), 'w') as fl:
                      fl.writelines(lines)  # write label.txt
          ## split image && line
          if train:
            slice_bboxes = get_slice_bboxes(
              image_width=size[0],
              image_height=size[1],
              slice_height=640,
              slice_width=640,
              overlap_height_ratio=0.2,
              overlap_width_ratio=0.2,
            )
            for slice in slice_bboxes:
              pol = Polygon([(slice[0], slice[1]), (slice[2] , slice[0]), (slice[2], slice[3]), (slice[0], slice[3])])
							for box in bboxes:
								if pol.intersects(box[1]): # contain  

  # Download
  dir = Path(yaml['path'])  # dataset root dir
  urls = ['https://github.com/ultralytics/yolov5/releases/download/v1.0/VisDrone2019-DET-train.zip',
          'https://github.com/ultralytics/yolov5/releases/download/v1.0/VisDrone2019-DET-val.zip',
          'https://github.com/ultralytics/yolov5/releases/download/v1.0/VisDrone2019-DET-test-dev.zip',
          'https://github.com/ultralytics/yolov5/releases/download/v1.0/VisDrone2019-DET-test-challenge.zip']
  download(urls, dir=dir, curl=True, threads=4)

  # Convert
  for d in 'VisDrone2019-DET-train', 'VisDrone2019-DET-val', 'VisDrone2019-DET-test-dev':
      visdrone2yolo(dir / d, train=d=='VisDrone2019-DET-train')  # convert VisDrone annotations to YOLO labels
